# README #

### How do I get set up? ###

You must have to install some package to run this project.  

1. pip install BeautifulSoup4
2. apt-get install python3-bs4
3. pip install scrapy-fake-useragent

after installing those package you are ready to go. just open your terminal and run a command which one you need.

### Available commands are: ###

1.  amrabondhu(https://www.amrabondhu.com/) Command: [scrapy crawl amrabondhu -o amrabondhu.json]
2. arkalekhalekhi(https://arkalekhalekhi.blogspot.com/) Command: [scrapy crawl arkalekhalekhi -o arkalekhalekhi.json]
3. bangalnama(https://bangalnama.wordpress.com//) Command: [scrapy crawl bangalnama -o bangalnama.json]
4. banglatech(https://banglatech.info/) Command: [scrapy crawl banglatech -o bangaltech.json]
5. bishorgo(http://www.bishorgo.com/) Command: [scrapy crawl bishorgo -o bishorgo.json]
6. cadetcollegeblog(https://cadetcollegeblog.com/) Command: [scrapy crawl cadetcollegeblog -o cadetcollegeblog.json]
7. chirasabuj(https://chirasabuj.blogspot.com/) Command: [scrapy crawl chirasabuj -o chirasabuj.json]
8. choturmatrik(http://www.choturmatrik.com/) Command: [scrapy crawl choturmatrik -o choturmatrik.json]
9. daaruk(https://www.daaruk.com/) Command: [scrapy crawl daaruk -o daaruk.json]
10. ishanerpunjomegh(https://ishanerpunjomegh.blogspot.com) Command: [scrapy crawl ishanerpunjomegh -o ishanerpunjomegh.json]
11. lokfolk(https://lokfolk.blogspot.com) Command: [scrapy crawl lokfolk -o lokfolk.json]
12. muktomona(https://blog.mukto-mona.com/) Command: [scrapy crawl muktomona -o muktomona.json]
13. projanmo(https://forum.projanmo.com/forum7.html) Command: [scrapy crawl projanmo -o projanmo.json]
14. sachalayatan(http://www.sachalayatan.com) Command: [scrapy crawl sachalayatan -o sachalayatan.json]
15. shodalap(http://shodalap.org/) Command: [scrapy crawl shodalap -o shodalap.json]
16. shopnobaz(http://shopnobaz.net/) Command: [scrapy crawl shopnobaz -o shopnobaz.json]
