import scrapy
import re
from bs4 import BeautifulSoup
# from BeautifulSoup import BeautifulSoup


class MmTimes(scrapy.Spider):
    name = "banglatech"

    start_urls = [
        'https://banglatech.info/',

    ]

    def parse(self, response):
        container = response.css('div#content')

        for quote in container.css('article.post'):
            
            end_point = str(quote.css('div.post-content header.entry-header h2.entry-title a::attr(href)').extract_first())

            if(end_point.find('http') != -1):
                    
                content_url = end_point
                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            else:
                
                # content_url = "https://www.daaruk.com/"+end_point
                content_url = end_point

                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            # content_url = content_url

            yield scrapy.Request(content_url, callback=self.parse_full_content, meta={'item': {
                
                'title': quote.css('div.post-content header.entry-header h2.entry-title a::text').extract(),
                'time': quote.css('div.post-content header.entry-header div.entry-meta span.posted-on span.published::text').extract(),
                # 'writer' : "Arka",
                'writer' : quote.css('div.post-content header.entry-header div.entry-meta span.posted-by a span.author-name::text').extract(),
                'content_link' :  content_url,
                
            }})

        next_page_url = str(response.css("div.ast-pagination div.nav-links a.next::attr(href)").extract_first())
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self,response):
        item = response.meta['item'] 
        html = response.css('main#main article.post div.entry-content p::text').extract()
        # html = response.css('div#page article.post div.entry-content input').xpath('@value').extract()
        
        item['full_description'] =  html
        
        return item