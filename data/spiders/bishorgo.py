import scrapy
import re
from bs4 import BeautifulSoup
# from BeautifulSoup import BeautifulSoup


class MmTimes(scrapy.Spider):
    name = "bishorgo"

    start_urls = [
        'http://www.bishorgo.com/',

    ]

    def parse(self, response):
        container = response.css('table#content')

        for quote in container.css('tr td div#main table.teaser'):
            
            # end_point = str(quote.css('tbody tr td.teaser-node-mc table tbody tr td h2.title a::attr(href)').extract_first())
            end_point = str(quote.css('tr td.teaser-node-mc table tr td h2.title a::attr(href)').extract_first())

            if(end_point.find('http') != -1):
                    
                content_url = end_point
                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            else:
                
                content_url = "http://www.bishorgo.com/"+end_point
                # content_url = end_point

                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            # content_url = content_url

            yield scrapy.Request(content_url, callback=self.parse_full_content, meta={'item': {
                
                'title': quote.css('tr td.teaser-node-mc table tr td h2.title a::text').extract(),
                'time': quote.css('tr td.teaser-node-mc table tr td span.submitted::text').extract(),
                'writer' : quote.css('tr td.teaser-node-mc table tr td span.submitted::text').extract(),
                'content_link' :  content_url,
                
            }})

        get_url_data = str(response.css("div#main div.item-list ul.pager li.pager-next a::attr(href)").extract_first())
        next_page_url = "http://www.bishorgo.com/"+get_url_data

        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self,response):
        item = response.meta['item'] 
        html = response.css('div#container tr td div#main table.node tr td div.content p::text').extract()
        # html = response.css('div#page article.post div.entry-content input').xpath('@value').extract()
        
        item['full_description'] =  html
        
        return item