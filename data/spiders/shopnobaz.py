import scrapy
import re
from bs4 import BeautifulSoup
# from BeautifulSoup import BeautifulSoup


class MmTimes(scrapy.Spider):
    name = "shopnobaz"

    start_urls = [
        'http://shopnobaz.net/',

    ]

    def parse(self, response):
        container = response.css('div#main')

        for quote in container.css('article.post'):
            
            end_point = str(quote.css('header.entry-header h2.entry-title a::attr(href)').extract_first())

            if(end_point.find('http') != -1):
                    
                content_url = end_point
                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            else:
                
                # content_url = "https://www.daaruk.com/"+end_point
                content_url = end_point

                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            # content_url = content_url

            yield scrapy.Request(content_url, callback=self.parse_full_content, meta={'item': {
                
                'title': quote.css('header.entry-header h2.entry-title a::text').extract(),
                'time': quote.css('footer.entry-meta-bar div.entry-meta span.date a time.entry-date::text').extract(),
                # 'writer' : " ",
                'writer' : quote.css('footer.entry-meta-bar div.entry-meta span.author a::text').extract(),
                'content_link' :  content_url,
                
            }})

        next_page_url = str(response.css("div#content ul.default-wp-page li.previous a::attr(href)").extract_first())
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self,response):
        item = response.meta['item'] 
        html = response.css('div#main article.post div.entry-content p::text').extract()
        # html = response.css('div#page article.post div.entry-content input').xpath('@value').extract()
        
        item['full_description'] =  html
        
        return item