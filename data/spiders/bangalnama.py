import scrapy
import re
from bs4 import BeautifulSoup
# from BeautifulSoup import BeautifulSoup


class MmTimes(scrapy.Spider):
    name = "bangalnama"

    start_urls = [
        'https://bangalnama.wordpress.com/',

    ]

    def parse(self, response):
        container = response.css('div#content')

        for quote in container.css('div.post'):
            
            end_point = str(quote.css('h2 a::attr(href)').extract_first())

            if(end_point.find('http') != -1):
                    
                content_url = end_point
                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            else:
                
                # content_url = "https://www.daaruk.com/"+end_point
                content_url = end_point

                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            # content_url = content_url

            yield scrapy.Request(content_url, callback=self.parse_full_content, meta={'item': {
                
                'title': quote.css('h2 a::text').extract(),
                'time': quote.css('p.date::text').extract(),
                # 'writer' : "Arka",
                'writer' : quote.css('p.date a::text').extract(),
                'content_link' :  content_url,
                
            }})

        next_page_url = str(response.css("div#content div.bottomnavigation a::attr(href)").extract_first())
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self,response):
        item = response.meta['item'] 
        html = response.css('div.entrytext p.MsoNormal span::text').extract()
        # html = response.css('div#page article.post div.entry-content input').xpath('@value').extract()
        
        item['full_description'] =  html
        
        return item