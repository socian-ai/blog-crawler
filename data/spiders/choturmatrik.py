import scrapy
import re
from bs4 import BeautifulSoup
# from BeautifulSoup import BeautifulSoup


class MmTimes(scrapy.Spider):
    name = "choturmatrik"

    start_urls = [
        'http://www.choturmatrik.com/',

    ]

    def parse(self, response):
        container = response.css('div#choturContent')

        for quote in container.css('div.node'):
            
            # end_point = str(quote.css('tbody tr td.teaser-node-mc table tbody tr td h2.title a::attr(href)').extract_first())
            end_point = str(quote.css('h2.title a::attr(href)').extract_first())

            if(end_point.find('http') != -1):
                    
                content_url = end_point
                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            else:
                
                content_url = "http://www.choturmatrik.com/"+end_point
                # content_url = end_point

                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            # content_url = content_url

            yield scrapy.Request(content_url, callback=self.parse_full_content, meta={'item': {
                
                'title': quote.css('h2.title a::text').extract(),
                'time': quote.css('div span.submitted::text').extract(),
                'writer' : quote.css('div span.submitted a::text').extract(),
                'content_link' :  content_url,
                
            }})

        get_the_perm = str(response.css("div.main-content-block div.item-list ul.pager li.pager-next a::attr(href)").extract_first())
        next_page_url = "http://www.choturmatrik.com/"+get_the_perm

        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self,response):
        item = response.meta['item'] 
        html = response.css('div.column-2 div.main-content-block div.node div.content p::text').extract()
        # html = response.css('div#page article.post div.entry-content input').xpath('@value').extract()
        
        item['full_description'] =  html
        
        return item