import scrapy
import re
from bs4 import BeautifulSoup
# from BeautifulSoup import BeautifulSoup


class MmTimes(scrapy.Spider):
    name = "ishanerpunjomegh"

    start_urls = [
        'https://ishanerpunjomegh.blogspot.com/2020/',
        'https://ishanerpunjomegh.blogspot.com/2019/',
        'https://ishanerpunjomegh.blogspot.com/2018/',
        'https://ishanerpunjomegh.blogspot.com/2017/',
        'https://ishanerpunjomegh.blogspot.com/2016/',
        'https://ishanerpunjomegh.blogspot.com/2015/',
        'https://ishanerpunjomegh.blogspot.com/2014/',
        'https://ishanerpunjomegh.blogspot.com/2013/',
        'https://ishanerpunjomegh.blogspot.com/2012/',
        'https://ishanerpunjomegh.blogspot.com/2011/',

    ]

    def parse(self, response):
        container = response.css('div.content')

        for quote in container.css('div.blog-posts div.date-outer'):
            
            end_point = str(quote.css('h3.post-title a::attr(href)').extract_first())

            if(end_point.find('http') != -1):
                    
                content_url = end_point
                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            else:
                
                content_url = "https://www.daaruk.com/"+end_point
                # content_url = end_point

                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            # content_url = content_url

            yield scrapy.Request(content_url, callback=self.parse_full_content, meta={'item': {
                
                'title': quote.css('h3.post-title a::text').extract(),
                'time': quote.css('h2.date-header span::text').extract(),
                'writer' : " ",
                # 'writer' : quote.css('div.entry-meta span.posted-by a span.author-name::text').extract(),
                'content_link' :  content_url,
                
            }})

        next_page_url = str(response.css("div.widget span#blog-pager-older-link a.blog-pager-older-link::attr(href)").extract_first())
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self,response):
        item = response.meta['item'] 
        html = response.css('div.post-outer div.post-body div::text').extract()
        # html = response.css('div#page article.post div.entry-content input').xpath('@value').extract()
        
        item['full_description'] =  html
        
        return item