import scrapy


class DayDaily(scrapy.Spider):
    name = "daydaily"

    start_urls = [
        # 'https://news-eleven.com/editorial',
        'http://thevoicemyanmar.com/news',
    ]

    def parse(self, response):
        # view_content = response.css('')
        root = response.css('section.latest-articles')
        root2 = root.css('div.row')[1]
        for quote in root2.css('div.row'):
            yield{

                'title': quote.css('div.row a::text').extract_first(),
                # 'content_link': quote.css('div.frontpage-title a::attr(href)').extract_first(),
                # 'next': response.css('li.pager-next a::attr(href)').extract_first()
            }


            # yield scrapy.Request(str(quote.css('div.frontpage-title a::attr(href)').extract_first()),
            #                      callback=self.parse_full_content, meta={'item': {
            #
            #         'title': quote.css('div.frontpage-title a::text').extract_first(),
            #         # 'content_link': quote.css('div.frontpage-title a::attr(href)').extract_first(),
            #         # 'next': response.css('div.item-list li.pager-next a::attr(href)').extract_first(),
            #     }})

        next_page_url = str(response.css('div.item-list li.pager-next a::attr(href)').extract_first())
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self, response):
        item = response.meta['item']
        item['full_description'] = response.css('div.field-item p::text').extract()
        return item
