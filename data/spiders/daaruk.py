import scrapy
import re
from bs4 import BeautifulSoup
# from BeautifulSoup import BeautifulSoup


class MmTimes(scrapy.Spider):
    name = "daaruk"

    start_urls = [
        'https://www.daaruk.com/',
        # 'https://www.amrabondhu.com/node?page=2',
        # 'https://cholontika.com/blog/2',
        
        # 'http://shodalap.org/page/2',
    
        # 'http://www.bishorgo.com/node?page=2',
        

    ]

    def parse(self, response):
        container = response.css('div.main_content_container main#main')

        for quote in container.css('article.post-outer-container'):
            
            end_point = str(quote.css('div.post-content div.post-title-container h3.post-title a::attr(href)').extract_first())

            if(end_point.find('http') != -1):
                    
                content_url = end_point
                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            else:
                
                content_url = "https://www.daaruk.com/"+end_point
                # content_url = end_point

                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            # content_url = content_url

            yield scrapy.Request(content_url, callback=self.parse_full_content, meta={'item': {
                
                'title': quote.css('div.post-content div.post-title-container h3.post-title a::text').extract(),
                'time': quote.css('div.post-content div.post-header-container a.timestamp-link time.published::text').extract(),
                'writer' : "Satarupa Biswas",
                # 'writer' : quote.css('div.art-Post-body div.art-Post-inner div.art-PostHeaderIcons a::text').extract(),
                'content_link' :  content_url,
                
            }})

        next_page_url = str(response.css("div.main_content_container div.blog-pager a::attr(href)").extract_first())
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self,response):
        item = response.meta['item'] 
        html = response.css('div.main_content_container div.post-body span::text').extract()
        
        item['full_description'] =  html
        
        return item