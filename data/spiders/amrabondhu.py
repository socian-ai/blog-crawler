import scrapy
import re
from bs4 import BeautifulSoup
# from BeautifulSoup import BeautifulSoup


class MmTimes(scrapy.Spider):
    name = "amrabondhu"

    start_urls = [
        'https://www.amrabondhu.com/',

    ]

    def parse(self, response):
        container = response.css('div#art-main')

        for quote in container.css('div.art-content div.art-Post'):
            
            end_point = str(quote.css('div.art-Post-inner h2 a::attr(href)').extract_first())

            if(end_point.find('http') != -1):
                    
                content_url = end_point
                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            else:
                
                content_url = "https://www.amrabondhu.com/"+end_point
                # content_url = end_point

                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            # content_url = content_url

            yield scrapy.Request(content_url, callback=self.parse_full_content, meta={'item': {
                
                'title': quote.css('div.art-Post-inner h2 a::text').extract(),
                'time': quote.css('div.art-Post-inner div.art-PostHeaderIcons::text').extract(),
                # 'writer' : "Arka",
                'writer' : quote.css('div.art-Post-inner div.art-PostHeaderIcons a::text').extract(),
                'content_link' :  content_url,
                
            }})

        get_url_data = str(response.css("div.art-content div.item-list ul.pager li.pager-next a::attr(href)").extract_first())
        next_page_url = "https://www.amrabondhu.com/"+get_url_data
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self,response):
        item = response.meta['item'] 
        html = response.css('div.art-content div.art-Post div.art-PostContent div.art-article p::text').extract()
        # html = response.css('div#page article.post div.entry-content input').xpath('@value').extract()
        
        item['full_description'] =  html
        
        return item