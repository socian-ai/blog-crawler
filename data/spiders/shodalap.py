import scrapy
import re
from bs4 import BeautifulSoup
# from BeautifulSoup import BeautifulSoup


class MmTimes(scrapy.Spider):
    name = "shodalap"

    start_urls = [
        'http://shodalap.org/',

    ]

    def parse(self, response):
        container = response.css('div#content-main div.entries-wrapper')

        for quote in container.css('div.type-post'):
            
            end_point = str(quote.css('div.entry h2.post-title a::attr(href)').extract_first())

            if(end_point.find('http') != -1):
                    
                content_url = end_point
                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            else:
                
                # content_url = "https://www.daaruk.com/"+end_point
                content_url = end_point

                print("------------------------------------"+str(content_url)+"---------------------------------------------------------------------------------------------------------------------")

            # content_url = content_url

            yield scrapy.Request(content_url, callback=self.parse_full_content, meta={'item': {
                
                'title': quote.css('div.entry h2.post-title a::text').extract(),
                'time': quote.css('div.entry div.date span.value-title').xpath('@title').extract(),
                # 'writer' : " ",
                'writer' : quote.css('div.entry div.post-meta p.post-author span a::text').extract(),
                'content_link' :  content_url,
                
            }})

        next_page_url = str(response.css("div#content-main div.post-nav p.previous a::attr(href)").extract_first())
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

    def parse_full_content(self,response):
        item = response.meta['item'] 
        html = response.css('div.post div.entry-content p::text').extract()
        # html = response.css('div#page article.post div.entry-content input').xpath('@value').extract()
        
        item['full_description'] =  html
        
        return item